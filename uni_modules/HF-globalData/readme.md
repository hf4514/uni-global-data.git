# HF-globalData
## 说明
  - 已经有vuex 为什么还要造轮子
	+ vuex 非常强大，但是操作也过于繁琐，有些时候我们只是需要一个公共状态的管理，并且更新视图，这都是 globalData 最初的原因
  - 未打包体积只有vuex的三十分一左右 (gData:1.32KB / vuex:37.3 KB)。
  - gData 只保证基本的响应式更改与数据获取和持久化存储等，主要实现Vuex 中的 store 模式。
  - 如需更强大的调试功能，建议还是使用 vuex
  - 使用：
	
  ````javaScript
  
	/* ------------ 设置属性与默认值 @/store/index.js------------ */
	
	// 将全局  globalData 值放置此处
	import siteinfo from '@/common/siteinfo.js'
	// 设置默认值与属性
	export const globalData = {
		g_siteinfo:siteinfo,
		g_userInfo:{ttt:333},
		g_test:"null",
		g_www:"wwww",
		g_cent:0
	}
	// 常量存储的 key 值
	export const setLongStorageData = ["g_userInfo","g_cent"]
	
	// 存放获取远程函数的回调,按需异步更新数据
	export const actionMap = {
		g_www(data,next){
			this.$http.post('rate').then(res=>{
				if(res.code === 200){
					/* 将数据结果通过next传递给全局 */
					next(res.data.rate)
				}
			})
		},
		g_userInfo(data,next){
			console.log("data")
			next('1')
			// return data
		}
	}
	
	
	
	/* -------------------------- END ------------------------- */

	// 获取数据 已使用 Vue.mixin 将数据混入 到了computed 中 可以通过 this 直接获取
	`
		ttt==>{{g_userInfo.ttt}}
		<br>
		g_userInfo==>{{g_userInfo}}
		<br>
		g_test ==> {{g_test}}
		<br>
		g_www ==> {{g_www}}
	`
	
	// 更改属性
	/* @注意  因为计算属性更改对多级嵌套的更改支持拦截不到 所以持久化存储的数据 
			如果更改的是子属性，持久存储回调拦截不到 建议使用 $gSet进行数据更改处理
	*/
	this.g_test = "onshoe"
	this.g_userInfo.ttt = "tttteeee"
	this.$gSet("g_userInfo.ttt","tttteeee")
	
  ````
  
  
 ## 示例文件
 ````
	// @/store/index.js
	// 将全局  globalData 值放置此处
	import {gData} from'@/uni_modules/HF-globalData/common/gData.js'
	
	
	
	// 设置默认值与属性
	export const globalData = {
		g_siteinfo: siteinfo,
		g_userInfo: {
			ttt: 333
		},
		g_test: "null",
		g_www: "wwww",
		g_cent: 0,
		g_arr: []
	}
	// 常量存储
	export const setLongStorageData = ["g_userInfo", "g_cent"]
	
	// 存放获取远程函数的回调
	export const actionMap = {
		g_www(data,next){
			this.$http.post('rate').then(res=>{
				if(res.code === 200){
					/* 将数据结果通过next传递给全局 */
					next(res.data.rate)
				}
			})
		},
		g_userInfo(data,next){
			console.log("data")
			next('1')
			// return data
		}
	}
	
	gData({globalData,setLongStorageData,actionMap})
 
 
	// mian.js
	import '@/store/index'
 
 
 ````
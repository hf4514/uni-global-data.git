// 将全局  globalData 值放置此处
import {gData} from'@/uni_modules/HF-globalData/common/gData.js'



// 设置默认值与属性
export const globalData = {
	g_siteinfo: siteinfo,
	g_userInfo: {
		ttt: 333
	},
	g_test: "null",
	g_www: "wwww",
	g_cent: 0,
	g_arr: []
}
// 常量存储
export const setLongStorageData = ["g_userInfo", "g_cent"]

// 存放获取远程函数的回调
export const actionMap = {
	g_www(data,next){
		this.$http.post('rate').then(res=>{
			if(res.code === 200){
				/* 将数据结果通过next传递给全局 */
				next(res.data.rate)
			}
		})
	},
	g_userInfo(data,next){
		console.log("data")
		next('1')
		// return data
	}
}

gData({globalData,setLongStorageData,actionMap})